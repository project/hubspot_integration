<?php

/**
 * @file
 * Provides views data for taxonomy.module.
 */

/**
 * Implements hook_views_data_alter().
 */
function hubspot_integration_views_data_alter(&$data) {
  $data['node_field_data']['hubspot_term_node_tid_depth'] = [
    'help' => t('Display content if it has the selected taxonomy terms, or children of the selected terms. Due to additional complexity, this has fewer options than the versions without depth.'),
    'real field' => 'nid',
    'argument' => [
      'title' => t('Hubspot : Has taxonomy term ID (with depth)'),
      'id' => 'hubspot_taxonomy_index_tid_depth',
      'accept depth modifier' => TRUE,
    ],
  ];

  $data['node_field_data']['hs_persona'] = [
    'title' => t('Hubspot persona'),
    'group' => t('Hubspot'),
    'help' => t('Sort events based on the sort configured.'),
    'sort' => [
      // 'field' => 'field_hs_persona',
      'id' => 'hs_persona',
    ],
  ];
}
