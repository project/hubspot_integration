(function ($, Drupal) {
  Drupal.behaviors.hubspotIntegration = {
    attach: function (context, settings) {
      $('html', context).once('hubspot_integration').each(function () {

        if (getCookie("hubspotutk") != '' && getCookie("hubspot_integration") == '' && settings.hubspot_integration.anonymous_contact == true) {
          var contactLimitReached = false;
          $.get('/hubspot_integration/ajax/is-limit-reached', function (data) {
            contactLimitReached = data.data.is_limit_reached;
          }, 'json');

          if (contactLimitReached == false) {
            var template = "" + settings.hubspot_integration.mail_template;
            var mail = template.replace('[hub_cookie]', getCookie("hubspotutk"));
            var _hsq = window._hsq = window._hsq || [];
            _hsq.push(["identify", {
              email: mail
            }]);
          }

          $.get('/hubspot_integration/ajax/is-contact', function (data) {
            if (data.data.is_contact == true) {
              setCookie('hubspot_integration', data.data.cookie);
            }
          }, 'json');
        }

        $('div[data-type="hubspot"]').each(function() {
          hbspt.forms.create({
            portalId: $(this).attr("data-portal-id"),
            formId: $(this).attr("data-form-id"),
            target: '#hs-' + $(this).attr("data-form-id"),
          });
        });

      })
    }
  };

  function getCookie(cname) {
    var name = cname + "=";
    var ca = document.cookie.split(';');
    for (var i = 0; i < ca.length; i++) {
      var c = ca[i];
      while (c.charAt(0) == ' ') {
        c = c.substring(1);
      }
      if (c.indexOf(name) == 0) {
        return c.substring(name.length, c.length);
      }
    }
    return "";
  }

  function setCookie(cname, cvalue, exdays) {
    var d = new Date();
    d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
    var expires = "expires=" + d.toUTCString();
    document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
  }
})(jQuery, Drupal);
