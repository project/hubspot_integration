<?php

namespace Drupal\hubspot_integration\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\hubspot_integration\Services\HubspotAPI;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 *
 */
class HubspotAPIAjaxController extends ControllerBase {

  /**
   * @var \Drupal\hubspot_integration\Services\HubspotAPI
   */
  protected $hubspotApi;

  /**
   * Constructs a HubspotAPIAjaxController object.
   *
   * @param \Drupal\hubspot_integration\Services\HubspotAPI $hubspotApi
   *   The HubspotApi service.
   */
  public function __construct(HubspotAPI $hubspotApi) {
    $this->hubspotApi = $hubspotApi;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('hubspot_integration.api')
    );
  }

  /**
   *
   */
  public function isContact() {
    $tids = $this->hubspotApi->getUserTids();

    return new JsonResponse([
      'data' => [
        'is_contact' => !empty($tids),
        'cookie' => 'hubspot_' . implode('_', $tids),
      ],
    ]);
  }

  /**
   *
   */
  public function isLimitReached() {
    $isLimitReached = FALSE;
    $config = $this->hubspotApi;
    $anonymousContactNumber = $config->getAnonymousContactNumber();
    $anonymousMaxContact = $config->getConfig('anonymous_max_contact');
    if ($anonymousMaxContact != NULL
      && $anonymousMaxContact != 0
      && $anonymousContactNumber < $anonymousMaxContact) {
      $isLimitReached = TRUE;
    }

    return new JsonResponse([
      'data' => [
        'is_limit_reached' => $isLimitReached,
      ],
    ]);
  }

}
