<?php

namespace Drupal\hubspot_integration\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Url;
use Drupal\hubspot_integration\Services\HubspotAPI;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Cookie;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Main controller.
 */
class HubspotIntegrationController extends ControllerBase {

  /**
   * The HubspotApi service.
   *
   * @var \Drupal\hubspot_integration\Services\HubspotAPI
   */
  protected $hubspotApi;

  /**
   * The request service.
   *
   * @var \Symfony\Component\HttpFoundation\Request
   */
  protected $request;

  /**
   * Constructs a HubspotIntegrationController object.
   *
   * @param \Drupal\hubspot_integration\Services\HubspotAPI $hubspotApi
   *   The HubspotApi service.
   * @param \Symfony\Component\HttpFoundation\RequestStack $request_stack
   *   The request stack.
   */
  public function __construct(HubspotAPI $hubspotApi, RequestStack $request_stack) {
    $this->hubspotApi = $hubspotApi;
    $this->request = $request_stack->getCurrentRequest();
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('hubspot_integration.api'),
      $container->get('request_stack')
    );
  }

  /**
   * Set Cookie hubspot_integration with persona_id value, and redirect after.
   *
   * @param string $persona_id
   *   The persona_id value.
   *
   * @return \Symfony\Component\HttpFoundation\RedirectResponse
   *   Return a redirect response in most cases.
   */
  public function setPersona($persona_id) {
    // Get query and query parameters.
    $query = $this->request->query;
    $queryParameters = $query->all();
    // Create the url.
    $url = Url::fromRoute('<front>')->toString();
    if (!empty($queryParameters['destination'])) {
      // Save destination parameter.
      $destination = '/' . $queryParameters['destination'];
      // Remove parameter destination from query and from list.
      $query->remove('destination');
      unset($queryParameters['destination']);
      // Set the final url.
      $url = Url::fromUserInput(
        $destination,
        [
          'query' => $queryParameters,
        ]
      )->toString();
    }
    // Create the response.
    $response = new RedirectResponse($url);
    // Create hubspot_integration cookie into the response if no already exist.
    if (!empty($persona_id)) {
      $cookie_key = 'hubspot_integration';
      $cookie_value = 'hubspot_' . $persona_id;
      $issetCookie = $this->request->cookies->get(
        $cookie_key
      );
      if (empty($issetCookie) ||  $issetCookie !== $cookie_value) {
        $response->headers->setCookie(
          Cookie::create($cookie_key, $cookie_value)
        );
      }
    }

    return $response;
  }

}
