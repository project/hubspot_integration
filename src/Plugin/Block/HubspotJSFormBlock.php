<?php

namespace Drupal\hubspot_integration\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Block\BlockPluginInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\hubspot_integration\Services\HubspotAPI;

/**
 * Provides a 'Hubspot JS Form' Block.
 *
 * @Block(
 *   id = "hubspot_integration_js_form_block",
 *   admin_label = @Translation("Hubspot JS Form"),
 *   category = @Translation("Hubspot JS Form"),
 * )
 */
class HubspotJSFormBlock extends BlockBase implements BlockPluginInterface, ContainerFactoryPluginInterface {

  /**
   * @var \Drupal\hubspot_integration\Services\HubspotAPI
   */
  protected $hubspotApi;

  /**
   * @param \Symfony\Component\DependencyInjection\ContainerInterface $container
   * @param array $configuration
   * @param string $plugin_id
   * @param mixed $plugin_definition
   *
   * @return static
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('hubspot_integration.api')
    );
  }

  /**
   * @param array $configuration
   * @param string $plugin_id
   * @param mixed $plugin_definition
   * @param \Drupal\hubspot_integration\Services\HubspotAPI $hubspotApi
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, HubspotAPI $hubspotApi) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->hubspotApi = $hubspotApi;
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    $config = $this->getConfiguration();
    $client_id = $this->hubspotApi->getConfig('client_id');
    $form_id = $config['hs_form_id'];

    $tag =
      '<script charset="utf-8" type="text/javascript" src="https://js.hsforms.net/forms/v2.js"></script>
      <script type="text/javascript">hbspt.forms.create({portalId: "' . $client_id . '",formId: "' . $form_id . '"});</script>';

    return [
      '#markup' => $tag,
      '#allowed_tags' => ['script'],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state) {
    $form = parent::blockForm($form, $form_state);
    $config = $this->getConfiguration();
    $form['hs_form_id'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Hubspot Form Id'),
      '#description' => $this->t('Hubspot form id to generate the form.'),
      '#default_value' => $config['hs_form_id'] ?? '',
      '#required' => TRUE,
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state) {
    parent::blockSubmit($form, $form_state);
    $values = $form_state->getValues();
    $this->configuration['hs_form_id'] = $values['hs_form_id'];
  }

}
