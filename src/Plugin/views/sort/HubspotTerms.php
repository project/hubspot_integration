<?php

namespace Drupal\hubspot_integration\Plugin\views\sort;

use Drupal\Core\Cache\CacheableDependencyInterface;
use Drupal\Core\Database\Connection;
use Drupal\hubspot_integration\Services\HubspotAPI;
use Drupal\views\Plugin\views\sort\SortPluginBase;
use Drupal\views\Plugin\ViewsHandlerManager;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Handle a Hubspot term sort.
 *
 * @ViewsSort("hs_persona")
 */
class HubspotTerms extends SortPluginBase implements CacheableDependencyInterface {

  /**
   * The hubspot api.
   *
   * @var \Drupal\hubspot_integration\Services\HubspotAPI
   */
  protected $hubspotApi;

  /**
   * The views handler manager.
   *
   * @var \Drupal\views\Plugin\ViewsHandlerManager
   */
  protected $joinHandler;

  /**
   * The database connection service.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $database;

  /**
   * THe handler boolean.
   *
   * @var bool
   */
  protected bool $isHandler;

  /**
   * Constructs a Handler object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\hubspot_integration\Services\HubspotAPI $hubspotApi
   *   The Hubspot API.
   * @param \Drupal\views\Plugin\ViewsHandlerManager $join_handler
   *   The views handler manager.
   * @param \Drupal\Core\Database\Connection $database
   *   The database connection service.
   */
  public function __construct(array $configuration,
                              $plugin_id,
                              $plugin_definition,
                              HubspotAPI $hubspotApi,
                              ViewsHandlerManager $join_handler,
                              Connection $database) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->hubspotApi = $hubspotApi;
    $this->isHandler = TRUE;
    $this->joinHandler = $join_handler;
    $this->database = $database;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('hubspot_integration.api'),
      $container->get('plugin.manager.views.join'),
      $container->get('database')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function usesGroupBy() {
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function query() {
    if (($cookie = $this->hubspotApi->getCookie('hubspot_integration'))
      && ($tids = explode('_', ltrim($cookie, 'hubspot_')))
      && ($sort = $this->hubspotApi->getTidsSort($tids))
      && (!empty($sort))
    ) {
      foreach ($sort as $term_weight_id => $term_weight) {
        $when .= sprintf('WHEN {nhsp}.hs_persona_target_id=%d THEN %d ', $term_weight_id, $term_weight);
      }
      $sub_query = $this->database->select('node__hs_persona', 'nhsp');
      $sub_query->addField('nhsp', 'entity_id');
      $sub_query->addExpression("MIN(CASE $when WHEN {nhsp}.hs_persona_target_id is NULL THEN 999 ELSE 1000 END)", 'hs_persona_weight');
      $sub_query->groupBy("nhsp.entity_id");

      if (!empty($when)) {
        $definition = [
          'table formula' => $sub_query,
          'type' => 'LEFT',
          'field' => 'entity_id',
          'left_table' => 'node_field_data',
          'left_field' => 'nid',
          'adjust' => TRUE,
        ];
        $join = $this->joinHandler->createInstance('standard', $definition);
        $rel = $this->query->addRelationship('hubspot_sort_node__hs_persona', $join, 'node_field_data');
        $this->query->addOrderBy(NULL,
            "ISNULL({hubspot_sort_node__hs_persona}.hs_persona_weight)",
            'ASC',
            'hubspot_sort_node__hs_persona_hs_persona_weight_is_null'
        );
        $this->query->addOrderBy(NULL, "{hubspot_sort_node__hs_persona}.hs_persona_weight", $this->options['order'], 'hs_persona_weight');

      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheContexts() {
    return ['cookies:hubspot_integration'];
  }

}
