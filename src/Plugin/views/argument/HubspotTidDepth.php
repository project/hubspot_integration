<?php

namespace Drupal\hubspot_integration\Plugin\views\argument;

use Drupal\Core\Database\Connection;
use Drupal\Core\Database\Query\Condition;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\hubspot_integration\Services\HubspotAPI;
use Drupal\taxonomy\VocabularyStorageInterface;
use Drupal\views\ViewExecutable;
use Drupal\views\Plugin\ViewsHandlerManager;
use Drupal\views\Plugin\views\argument\ArgumentPluginBase;
use Drupal\views\Plugin\views\display\DisplayPluginBase;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Hubspot Integration Taxonomy tid argument.
 *
 * This handler is actually part of the node table and has some restrictions,
 * because it uses a subquery to find nodes with.
 *
 * @ingroup views_argument_handlers
 *
 * @ViewsArgument("hubspot_taxonomy_index_tid_depth")
 */
class HubspotTidDepth extends ArgumentPluginBase implements ContainerFactoryPluginInterface {

  /**
   * The vocabulary storage.
   *
   * @var \Drupal\taxonomy\VocabularyStorageInterface
   */
  protected $vocabularyStorage;

  /**
   * The term storage service.
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  protected $termStorage;

  /**
   * Database Service Object.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $database;

  /**
   * The Hubspot API.
   *
   * @var \Drupal\hubspot_integration\Services\HubspotAPI
   */
  protected $hubspotApi;

  /**
   * Constructs a new Tid instance.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.   *.
   * @param \Drupal\taxonomy\VocabularyStorageInterface $vocabulary_storage
   *   The vocabulary storage.
   * @param \Drupal\Core\Entity\EntityStorageInterface $termStorage
   *   The Hubspot API.
   * @param \Drupal\Core\Database\Connection $database
   *   The database connection service.
   * @param \Drupal\views\Plugin\ViewsHandlerManager $join_handler
   *   The join handler.
   * @param \Drupal\hubspot_integration\Services\HubspotAPI $hubspotApi
   *   The hubspot API service.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, VocabularyStorageInterface $vocabulary_storage, EntityStorageInterface $termStorage, Connection $database, ViewsHandlerManager $join_handler, HubspotAPI $hubspotApi) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $termStorage);
    $this->vocabularyStorage = $vocabulary_storage;
    $this->termStorage = $termStorage;
    $this->database = $database;
    $this->joinHandler = $join_handler;
    $this->hubspotApi = $hubspotApi;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager')->getStorage('taxonomy_vocabulary'),
      $container->get('entity_type.manager')->getStorage('taxonomy_term'),
      $container->get('database'),
      $container->get('plugin.manager.views.join'),
      $container->get('hubspot_integration.api')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function init(ViewExecutable $view, DisplayPluginBase $display, array &$options = NULL) {
    parent::init($view, $display, $options);

    // @todo Remove the legacy code.
    // Convert legacy vids option to machine name vocabularies.
    if (!empty($this->options['vids'])) {
      $vocabularies = taxonomy_vocabulary_get_names();
      foreach ($this->options['vids'] as $vid) {
        if (isset($vocabularies[$vid], $vocabularies[$vid]->machine_name)) {
          $this->options['vocabularies'][$vocabularies[$vid]->machine_name] = $vocabularies[$vid]->machine_name;
        }
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  protected function defineOptions() {
    $options = parent::defineOptions();
    $options['depth'] = ['default' => 0];
    $options['break_phrase'] = ['default' => FALSE];
    $options['use_taxonomy_term_path'] = ['default' => FALSE];
    $options['anyall'] = ['default' => ','];
    $options['limit'] = ['default' => FALSE];
    $options['vids'] = ['default' => []];
    $options['query'] = ['default' => 0];

    return $options;
  }

  /**
   * {@inheritdoc}
   */
  public function buildOptionsForm(&$form, FormStateInterface $form_state) {
    $hubspotApi = $this->hubspotApi;
    $form['limit'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Limit terms by vocabulary'),
      '#default_value' => $this->options['limit'],
    ];
    $options = [];
    $vocabularies = $this->vocabularyStorage->loadMultiple();
    foreach ($vocabularies as $voc) {
      // Show only mapped vocabulary.
      if ($hubspotApi->isVocabularyMapped($voc->id())) {
        $options[$voc->id()] = $voc->label();
      }
    }
    $form['vids'] = [
      '#type' => 'checkboxes',
      '#title' => $this->t('Vocabularies'),
      '#description' => $this->t('This field only show vocabularies mapped with Hubspot'),
      '#options' => $options,
      '#default_value' => $this->options['vids'],
      '#states' => [
        'visible' => [
          ':input[name="options[limit]"]' => ['checked' => TRUE],
        ],
      ],
    ];
    $form['query'] = [
      '#type' => 'radios',
      '#title' => $this->t('Fallback'),
      '#description' => $this->t('Behaviour when persona is found.'),
      '#default_value' => $this->options['query'],
      '#options' => [
        0 => $this->t('Exclude content without persona'),
        1 => $this->t('Include content without persona'),
      ],
    ];
    parent::buildOptionsForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitOptionsForm(&$form, FormStateInterface $form_state, &$options = []) {
    // Filter unselected items so we don't unnecessarily store giant arrays.
    $options['vids'] = array_filter($options['vids']);
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheContexts() {
    return ['cookies:hubspot_integration'];
  }

  /**
   * The query method.
   */
  public function query($group_by = FALSE) {
    if (($tids = $this->argument) && ($tids != 'no-persona')) {
      $tids = explode(',', $tids);
      $this->ensureMyTable();
      if (!is_array($tids)) {
        $break = static::breakString($this->argument);
        if ($break->value === [-1]) {
          return FALSE;
        }
        else {
          $tids = $break->value;
        }
      }
      $operator = (count($tids) > 1) ? 'IN' : '=';
      $definition = [
        'table' => 'node__hs_persona',
        'type' => 'LEFT',
        'field' => 'entity_id',
        'left_table' => 'node_field_data',
        'left_field' => 'nid',
      ];
      $join = $this->joinHandler->createInstance('standard', $definition);
      $rel = $this->query->addRelationship('hubspot_filter_node__hs_persona', $join, 'node_field_data');
      $this->query->addTable('node__hs_persona', $rel, $join);
      $condition = (new Condition('OR'))
        ->condition('hubspot_filter_node__hs_persona.hs_persona_target_id', $tids, $operator);
      if ($this->options['query'] == 1) {
        $condition->condition('hubspot_filter_node__hs_persona.hs_persona_target_id', NULL, 'IS NULL');
      }
      $this->query->addWhere(0, $condition);
    }
  }

}
