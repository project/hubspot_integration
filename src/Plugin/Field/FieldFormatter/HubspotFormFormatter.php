<?php

namespace Drupal\hubspot_integration\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FormatterBase;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\hubspot_integration\Services\HubspotAPI;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Plugin implementation of the 'hubspot_integration_form_formatter' formatter.
 *
 * @FieldFormatter(
 *   id = "hubspot_integration_form_formatter",
 *   label = @Translation("Hubspot Form formatter"),
 *   field_types = {
 *     "hubspot_integration_form"
 *   }
 * )
 */
class HubspotFormFormatter extends FormatterBase {

  /**
   * The Hubspot API service.
   *
   * @var \Drupal\hubspot_integration\Services\HubspotAPI
   */
  protected $hubspotApi;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $plugin_id,
      $plugin_definition,
      $configuration['field_definition'],
      $configuration['settings'],
      $configuration['label'],
      $configuration['view_mode'],
      $configuration['third_party_settings'],
      $container->get('hubspot_integration.api')
    );
  }

  /**
   * @param $plugin_id
   * @param $plugin_definition
   * @param \Drupal\Core\Field\FieldDefinitionInterface $field_definition
   * @param array $settings
   * @param $label
   * @param $view_mode
   * @param array $third_party_settings
   * @param \Drupal\hubspot_integration\Services\HubspotAPI $hubspotApi
   */
  public function __construct($plugin_id, $plugin_definition, FieldDefinitionInterface $field_definition, array $settings, $label, $view_mode, array $third_party_settings, HubspotAPI $hubspotApi) {
    parent::__construct($plugin_id, $plugin_definition, $field_definition, $settings, $label, $view_mode, $third_party_settings);
    $this->hubspotApi = $hubspotApi;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    /*$form['hs_form_id'] = [
    '#title' => $this->t('Hubspot Form ID'),
    '#type' => 'textfield',
    '#description' => $this->t('Hubspot Form ID.'),
    '#default_value' => $this->getSetting('hs_form_id'),
    ];*/
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $element = [];
    $client_id = $this->hubspotApi->getConfig('client_id');
    foreach ($items as $delta => $item) {
      // Render each element as markup.
      $element[$delta] = [
        '#markup' => '<div id="hs-' . $item->hs_form_id . '" data-type="hubspot" data-portal-id="' . $client_id . '" data-form-id="' . $item->hs_form_id . '"></div>',
      ];
    }
    return $element;
  }

}
