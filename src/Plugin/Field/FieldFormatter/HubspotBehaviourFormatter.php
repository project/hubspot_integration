<?php

namespace Drupal\hubspot_integration\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FormatterBase;
use Drupal\hubspot_integration\Services\HubspotAPI;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Plugin implementation of the 'hubspot_behaviour_formatter' formatter.
 *
 * @FieldFormatter(
 *   id = "hubspot_behaviour_formatter",
 *   label = @Translation("Hubspot Behaviour - Formatter"),
 *   description = @Translation("Hubspot Behaviour - Formatter"),
 *   field_types = {
 *     "hubspot_behaviour_field",
 *   }
 * )
 */
class HubspotBehaviourFormatter extends FormatterBase {

  /**
   * @var \Drupal\hubspot_integration\Services\HubspotAPI
   */
  protected HubspotAPI $hubspotApi;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $plugin_id,
      $plugin_definition,
      $configuration['field_definition'],
      $configuration['settings'],
      $configuration['label'],
      $configuration['view_mode'],
      $configuration['third_party_settings'],
      $container->get('hubspot_integration.api')
      );
  }

  /**
   * @param string $plugin_id
   * @param mixed $plugin_definition
   * @param \Drupal\Core\Field\FieldDefinitionInterface $field_definition
   * @param array $settings
   * @param $label
   * @param $view_mode
   * @param array $third_party_settings
   * @param \Drupal\hubspot_integration\Services\HubspotAPI $hubspotApi
   */
  public function __construct($plugin_id, $plugin_definition, FieldDefinitionInterface $field_definition, array $settings, $label, $view_mode, array $third_party_settings, HubspotAPI $hubspotApi) {
    parent::__construct($plugin_id, $plugin_definition, $field_definition, $settings, $label, $view_mode, $third_party_settings);
    $this->hubspotApi = $hubspotApi;
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $elements = [];
    foreach ($items as $delta => $item) {
      $elements[$delta] = [
        'hs_behaviour' => [
          '#markup' => 'No access',
        ],
        // Add more content.
      ];
    }

    return $elements;
  }

}
