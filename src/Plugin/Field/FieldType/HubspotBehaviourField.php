<?php

namespace Drupal\hubspot_integration\Plugin\Field\FieldType;

use Drupal\Core\Field\FieldItemBase;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\TypedData\DataDefinition;

/**
 * Provides a field type of HubspotBehaviourField.
 *
 * @FieldType(
 *   id = "hubspot_behaviour_field",
 *   label = @Translation("Hubspot behaviour field"),
 *   default_widget = "hubspot_behaviour_widget",
 *   default_formatter = "hubspot_behaviour_formatter",
 *   category = @Translation("Hubspot Integration"),
 * )
 */
class HubspotBehaviourField extends FieldItemBase {

  /**
   *
   */
  public static function propertyDefinitions(FieldStorageDefinitionInterface $field_definition) {
    return [
      'hs_behaviour' => DataDefinition::create('string')
        ->setLabel(t('Hubspot Behaviour'))
        ->setDescription(t('Indicate how the referenced entities appear.')),
    ];
  }

  /**
   *
   */
  public static function schema(FieldStorageDefinitionInterface $field_definition) {
    $columns = [
      'hs_behaviour' => [
        'description' => 'Hubspot Behaviour.',
        'type' => 'varchar',
        'length' => 255,
      ],
    ];
    return [
      'columns' => $columns,
      'indexes' => [],
      'foreign keys' => [],
    ];
  }

}
