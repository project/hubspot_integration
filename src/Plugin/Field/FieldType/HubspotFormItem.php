<?php

namespace Drupal\hubspot_integration\Plugin\Field\FieldType;

use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Field\FieldItemBase;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\TypedData\DataDefinition;

/**
 * Provides a field type of hubspot_integration_form.
 *
 * @FieldType(
 *   id = "hubspot_integration_form",
 *   label = @Translation("Hubspot Form field"),
 *   default_formatter = "hubspot_integration_form_formatter",
 *   default_widget = "hubspot_integration_form_widget",
 *   category = @Translation("Hubspot Integration"),
 * )
 */
class HubspotFormItem extends FieldItemBase {

  /**
   * {@inheritdoc}
   */
  public static function schema(FieldStorageDefinitionInterface $field_definition) {
    return [
      // Columns contains the values that the field will store.
      'columns' => [
        // List the values that the field will save. This
        // field will only save a single value, 'value'.
        'hs_form_id' => [
          'type' => 'varchar',
          'length' => 255,
          'not null' => TRUE,
        ],
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public static function propertyDefinitions(FieldStorageDefinitionInterface $field_definition) {
    $properties = [];
    $properties['hs_form_id'] = DataDefinition::create('string')->setLabel(t('Hubspot Form ID'));

    return $properties;
  }

  /**
   * {@inheritdoc}
   */
  public function isEmpty() {
    $value = $this->get('hs_form_id')->getValue();
    return $value === NULL || $value === '';
  }

  /**
   * {@inheritdoc}
   */
  public static function defaultFieldSettings() {
    return [
      // Declare a single setting, 'size', with a default
      // value of 'large'.
      'hs_form_id' => '',
    ] + parent::defaultFieldSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function fieldSettingsForm(array $form, FormStateInterface $form_state) {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    // Some fields above.
    $fields['hs_form_id'] = BaseFieldDefinition::create('hubspot_integration_form')
      ->setLabel(t('Hubspot Form ID'))
      ->setDescription(t('Hubspot Form ID.'));

    return $fields;
  }

}
