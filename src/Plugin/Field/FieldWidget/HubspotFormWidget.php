<?php

namespace Drupal\hubspot_integration\Plugin\Field\FieldWidget;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\WidgetBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Plugin implementation of the 'hubspot_integration_form_widget' widget.
 *
 * @FieldWidget(
 *   id = "hubspot_integration_form_widget",
 *   module = "hubspot_integration",
 *   label = @Translation("Hubspot Integration form"),
 *   field_types = {
 *     "hubspot_integration_form"
 *   }
 * )
 */
class HubspotFormWidget extends WidgetBase {

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {
    $value = $items[$delta]->hs_form_id ?? '';
    $element += [
      '#type' => 'textfield',
      '#default_value' => $value,
      '#size' => 40,
      '#maxlength' => 40,
      '#element_validate' => [
        [static::class, 'validate'],
      ],
    ];

    return ['hs_form_id' => $element];
  }

  /**
   * Validate the hubspot form_id.
   */
  public static function validate($element, FormStateInterface $form_state) {
    $value = $element['#value'];
    if (strlen($value) == 0) {
      $form_state->setValueForElement($element, '');
    }
  }

}
