<?php

namespace Drupal\hubspot_integration\Plugin\Field\FieldWidget;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\WidgetBase;
use Drupal\Core\Field\WidgetInterface;
use Drupal\Core\Form\FormStateInterface;

/**
 * Plugin implementation of the 'hubspot_behaviour_widget' widget.
 *
 * @FieldWidget(
 *   id = "hubspot_behaviour_widget",
 *   label = @Translation("Hubspot Behaviour widget"),
 *   description = @Translation("Hubspot Behaviour - Widget"),
 *   field_types = {
 *     "hubspot_behaviour_field",
 *   },
 * )
 */
class HubspotBehaviourWidget extends WidgetBase implements WidgetInterface {

  /**
   * {@inheritdoc}
   */
  protected function getSettingOptions($setting_name) {
    $options = [];
    if ($setting_name == 'hs_behaviour') {
      $options = [
        'default' => 'Default',
        'highlight' => 'Highlighting',
        'filtering' => 'Filtering with callback',
      ];
    }

    return !empty($options) ? $options : NULL;
  }

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return [
      'hs_behaviour' => 'default',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    return [];
  }

  /**
   * {@inheritdoc}
   *
   * @see \Drupal\content_translation\Controller\ContentTranslationController::prepareTranslation()
   *   Uses a similar approach to populate a new translation.
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {
    $value = $items[$delta]->hs_behaviour ?? '';
    $element = [
      '#type' => 'radios',
      '#options' => $this->getSettingOptions('hs_behaviour'),
      '#title' => $this->t('Hubspot behaviour'),
      '#default_value' => $value,
      '#required' => TRUE,
    ];

    return ['hs_behaviour' => $element];
  }

}
