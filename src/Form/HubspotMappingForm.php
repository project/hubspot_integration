<?php

namespace Drupal\hubspot_integration\Form;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\hubspot_integration\Services\HubspotAPI;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Configures Hubspot mapping for this site.
 */
class HubspotMappingForm extends ConfigFormBase {

  /**
   * The config factory service.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface|null
   */
  protected $configFactory = NULL;

  /**
   * The Hubspot API service.
   *
   * @var \Drupal\hubspot_integration\Services\HubspotAPI|null
   */
  protected $hubspotApi = NULL;

  /**
   * The messenger service.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * The entity type manager service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The properties.
   *
   * @var array
   */
  protected $properties = [];

  /**
   * The mapping array.
   *
   * @var array|array[]|\Drupal\Component\Render\MarkupInterface|\Drupal\hubspot_integration\Services\unknown[]|mixed|string|null
   */
  protected $mapping = [];

  /**
   * The managed types.
   *
   * @var array
   */
  protected $managedTypes = ['enumeration'];

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('hubspot_integration.api'),
      $container->get('messenger'),
      $container->get('entity_type.manager')
    );
  }

  /**
   * Constructs a \Drupal\HubspotIntegration\Form\HubspotAdminForm object.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $configFactory
   *   The config factory service.
   * @param \Drupal\hubspot_integration\Services\HubspotAPI $hubspotApi
   *   The Hubspot API service.
   * @param \Drupal\Core\Messenger\MessengerInterface $messenger
   *   The messenger service.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   The entity type manager service.
   */
  public function __construct(ConfigFactoryInterface $configFactory, HubspotAPI $hubspotApi, MessengerInterface $messenger, EntityTypeManagerInterface $entityTypeManager) {
    $this->configFactory = $configFactory;
    $this->hubspotApi = $hubspotApi;
    $this->messenger = $messenger;
    $this->entityTypeManager = $entityTypeManager;
    $properties = $this->hubspotApi->getContactProperties();
    if ($properties) {
      foreach ($properties as $property) {
        $this->properties[$property->name] = $property;
      }
    }
    $this->mapping = $this->hubspotApi->getMapping();
    parent::__construct($configFactory);
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'hubspot_integration_admin_mapping_form';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['hubspot_integration.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $settings = $this->config('hubspot_integration.settings');
    if (empty($settings->get('token_private_app'))) {
      $this->messenger->addError($this->t('No token yet.'));
      return $form;
    }
    if ($form_state->isSubmitted()) {
      $this->mapping = HubspotMappingForm::getMappingFromFormState($form_state);
    }
    $options = [];
    foreach ($this->properties as $property_name => $property) {
      if (in_array($property->type, $this->managedTypes)) {
        $options[$property_name] = $property->label . ' - (' . $property->type . ')';
      }
    }
    $form['container'] = [
      '#type' => 'container',
      '#attributes' => ['id' => 'select-container'],
    ];
    $form['container']['property_select'] = [
      '#type' => 'select',
      '#title' => 'Select a property',
      '#options' => $options,
      '#default_value' => NULL,
    ];
    $form['container']['add_item'] = [
      '#type'   => 'button',
      '#value'  => $this->t('Add another item'),
      '#name' => 'add_item',
      '#ajax'   => [
        'callback' => '::addProperty',
        'wrapper'  => 'mapping-container',
      ],
    ];
    $form['mapping'] = [
      '#type' => 'container',
      '#attributes' => ['id' => 'mapping-container'],
    ];
    foreach ($this->mapping as $property_name => $mapping) {
      $property = $this->properties[$property_name];
      $form['mapping']['details__' . $property->name] = [
        '#type' => 'details',
        '#title' => $property->label . ' (' . $property->type . ')',
        '#description' => $property->description,
        '#open' => FALSE,
      ];
      $form['mapping']['details__' . $property_name] += $this->mappingItemForm($property, $form_state);
    }
    if ($triggering_element = $form_state->getUserInput()['_triggering_element_name']) {
      if ($triggering_element == 'add_item') {
        $property_name = $form_state->getValue('property_select');
      }
      elseif (stristr($triggering_element, 'vocabulary__')) {
        $property_name = str_replace('vocabulary__', '', $form_state->getUserInput()['_triggering_element_name']);
      }
      $property = $this->properties[$property_name];
      $form['mapping']['details__' . $property->name] = [
        '#type' => 'details',
        '#title' => $property->label . ' (' . $property->type . ')',
        '#description' => $property->description,
        '#open' => TRUE,
      ];
      $form['mapping']['details__' . $property_name] += $this->mappingItemForm($property, $form_state);
      $form_state->setRebuild();
    }

    return parent::buildForm($form, $form_state);
  }

  /**
   * Build a form item depending on hubspot item type.
   *
   * @param $property
   *   The properties.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state object.
   *
   * @return array
   *   The item options.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  protected function mappingItemForm($property, FormStateInterface $form_state) {
    $item = [];
    switch ($property->type) {
      case 'enumeration':
        $item = $this->mappingItemEnumerationForm($property, $form_state);
        break;
      // @todo handler ?
      case 'bool':
        // @todo handler ?
      case 'string':
        // @todo handler ?
      case 'phone_number':
        // @todo handler ?
      case 'datetime':
      default:
    }

    return $item;
  }

  /**
   * Build a form item depending on hubspot item type.
   *
   * @param $property
   *   The properties.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state object.
   *
   * @return array
   *   The item options.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  protected function mappingItemEnumerationForm($property, FormStateInterface $form_state) {
    $item_opts = [];
    $vocabularies = $this->entityTypeManager->getStorage('taxonomy_vocabulary')->loadMultiple();
    $vocabulariesList = [];
    foreach ($vocabularies as $vid => $vocablary) {
      $vocabulariesList[$vid] = $vocablary->get('name');
    }
    $item_opts['vocabulary__' . $property->name] = [
      '#type' => 'select',
      '#title' => $this->t('Please select a vocabulary to map'),
      '#options' => [0 => $this->t('-- None --')] + $vocabulariesList,
      '#default_value' => $this->mapping[$property->name]['#vocabulary'],
      '#ajax' => [
        'callback' => '::populateMappingOptions',
        'wrapper' => 'mapping__' . $property->name,
        'event' => 'change',
        'method' => 'replace',
        'effect' => 'fade',
      ],
    ];
    $item_opts['mapping__' . $property->name] = [
      '#type' => 'container',
      '#attributes' => ['id' => 'mapping__' . $property->name],
    ];
    foreach ($property->options as $option) {
      // Hubspot label & values are human readable, we have to create a machine
      // name to avoid the forms items key issues.
      $option->value = $this->hubspotApi->machineName($option->value);
      $item_opts['mapping__' . $property->name]['hubspot__' . $property->name . '__' . $option->value] = [
        '#type' => 'entity_autocomplete',
        '#target_type' => 'taxonomy_term',
        '#title' => $option->label,
        '#description' => $option->description,
        '#default_value' => (isset($this->mapping[$property->name]['#terms'][$option->value])) ? $this->entityTypeManager->getStorage('taxonomy_term')->load($this->mapping[$property->name]['#terms'][$option->value]) : NULL,
        '#tags' => FALSE,
        '#selection_settings' => [
          'target_bundles' => [(isset($this->mapping[$property->name]['#vocabulary'])) ? $this->mapping[$property->name]['#vocabulary'] : NULL],
        ],
        '#weight' => '0',
      ];
    }

    return $item_opts;
  }

  /**
   * Ajax callback when selecting a mapping option.
   *
   * @param array $form
   *   The form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state object.
   *
   * @return array
   *   The form field.
   */
  public function populateMappingOptions(array &$form, FormStateInterface $form_state) {
    $property_name = str_replace('vocabulary__', '', $form_state->getUserInput()['_triggering_element_name']);
    return $form['mapping']['details__' . $property_name]['mapping__' . $property_name];
  }

  /**
   * Ajax callback when add a new mapping item form.
   *
   * @param array $form
   *   The form array.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state object.
   *
   * @return array
   *   The form field.
   */
  public function addProperty(array &$form, FormStateInterface $form_state) {
    return $form['mapping'];
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);
    $this->mapping = HubspotMappingForm::getMappingFromFormState($form_state);
    $config = $this->config('hubspot_integration.settings');
    $config->set('mapping', $this->mapping);
    $config->save();
  }

  /**
   * Get the mapping.
   */
  protected static function getMappingFromFormState(FormStateInterface $form_state) {
    $mapping = [];
    foreach ($form_state->getValues() as $key => $value) {
      if (stristr($key, 'vocabulary__')) {
        $property_str = str_replace('vocabulary__', '', $key);
        $arr = explode('__', $property_str);
        $property_name = $arr[0];
        $mapping[$property_name]['#vocabulary'] = $value;
      }
      elseif (stristr($key, 'hubspot__')) {
        $property_str = str_replace('hubspot__', '', $key);
        $arr = explode('__', $property_str);
        $property_name = $arr[0];
        $option_value = $arr[1];
        $mapping[$property_name]['#terms'][$option_value] = $value;
      }
    }

    return $mapping;
  }

}
