<?php

namespace Drupal\hubspot_integration\Form;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\hubspot_integration\Services\HubspotAPI;

/**
 * Configures Hubspot settings for this site.
 */
class HubspotAdminForm extends ConfigFormBase {

  /**
   * The Hubspot API service.
   *
   * @var \Drupal\hubspot_integration\Services\HubspotAPI
   */
  protected $hubspotApi;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('hubspot_integration.api')
    );
  }

  /**
   * Constructs a \Drupal\HubspotIntegration\Form\HubspotAdminForm object.
   */
  public function __construct(ConfigFactoryInterface $config_factory, HubspotAPI $hubspotApi) {
    parent::__construct($config_factory);
    $this->hubspotApi = $hubspotApi;
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'hubspot_integration_admin_form';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['hubspot_integration.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('hubspot_integration.settings');
    $form['client_id'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Client ID'),
      '#default_value' => $config->get('client_id'),
    ];
    $form['token_private_app'] = [
      '#type' => 'textfield',
      '#title' => $this->t("Token Private App"),
      '#default_value' => $config->get('token_private_app'),
    ];
    $form['demo_mode'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Demo mode (disable all cached and session limitation, usefull for a demo and see changes immediatly)'),
      '#default_value' => $config->get('demo_mode'),
    ];
    $form['anonymous_contact'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Automatically create new contacts for anonymous visitors'),
      '#default_value' => $config->get('anonymous_contact'),
    ];
    $form['anonymous_mail_template'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Mail Template for anonymous contacts.'),
      '#default_value' => $this->hubspotApi->getAnonymousContactMailTemplate(),
      '#description' => $this->t('Please add the [hub_cookie] token in order to use it.'),
    ];
    $form['anonymous_max_contact'] = [
      '#type' => 'textfield',
      '#attributes' => [
    // Insert space before attribute name :)
        ' type' => 'number',
      ],
      '#title' => $this->t('Max anonymous contacts.'),
      '#description' => $this->t('Let empty if no limit.'),
      '#default_value' => $config->get('anonymous_max_contact'),
      '#states' => [
        'visible' => [
          ':input[name="anonymous_contact"]' => ['checked' => TRUE],
        ],
      ],
      '#suffix' => $this->t('Actually %number anonymous contact(s) created.', ['%number' => $this->hubspotApi->getAnonymousContactNumber(TRUE)]),
    ];
    $form['notify_exceeded_contacts'] = [
      '#type' => 'textarea',
      '#title' => $this->t('If the number of contacts is exceeded, send notification to :'),
      '#description' => $this->t('List of emails separated by a semicolon'),
      '#default_value' => $config->get('notify_exceeded_contacts'),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);
    $config = $this->config('hubspot_integration.settings');
    $config->set('client_id', $form_state->getValue('client_id'));
    $config->set('token_private_app', $form_state->getValue('token_private_app'));
    $config->set('demo_mode', $form_state->getValue('demo_mode'));
    $config->set('anonymous_contact', $form_state->getValue('anonymous_contact'));
    $config->set('anonymous_mail_template', $form_state->getValue('anonymous_mail_template'));
    $config->set('anonymous_max_contact', $form_state->getValue('anonymous_max_contact'));
    $config->set('notify_exceeded_contacts', $form_state->getValue('notify_exceeded_contacts'));

    $config->save();
  }

}
