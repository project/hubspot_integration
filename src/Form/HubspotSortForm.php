<?php

namespace Drupal\hubspot_integration\Form;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\hubspot_integration\Services\HubspotAPI;
use Drupal\taxonomy\Entity\Vocabulary;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Configures Hubspot settings for this site.
 */
class HubspotSortForm extends ConfigFormBase {

  /**
   * The Hupspot API service.
   *
   * @var \Drupal\hubspot_integration\Services\HubspotAPI
   */
  protected $hubspotApi;

  /**
   * The sort parameter.
   *
   * @var array
   */
  protected $sort;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('hubspot_integration.api')
    );
  }

  /**
   * Constructs a HubspotSortForm object.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory service.
   * @param \Drupal\hubspot_integration\Services\HubspotAPI $hubspotApi
   *   The Hupspot API service.
   */
  public function __construct(ConfigFactoryInterface $config_factory, HubspotAPI $hubspotApi) {
    parent::__construct($config_factory);
    $this->hubspotApi = $hubspotApi;
    $this->sort = $this->hubspotApi->getSort();
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'hubspot_integration_sort_form';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['hubspot_integration.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    // Get property options.
    $mappedVocabularies = $this->hubspotApi->getMappedVocabularies();
    foreach ($this->sort as $vocabularyId => $sort) {
      unset($mappedVocabularies[$vocabularyId]);
    }
    // Build Add Property button.
    if (!empty($mappedVocabularies)) {
      $form['container'] = [
        '#type' => 'container',
      ];
      $form['container']['property_select'] = [
        '#type' => 'select',
        '#title' => $this->t('Select a property'),
        '#options' => $mappedVocabularies,
        '#default_value' => NULL,
      ];
      $addItemButtonValue = $this->t('Add new item');
      if (!empty($this->sort)) {
        $addItemButtonValue = $this->t('Add another item');
      }
      $form['container']['add_item'] = [
        '#type'   => 'submit',
        '#value'  => $addItemButtonValue,
        '#name' => 'add_item',
        '#button_type' => 'primary',
      ];
    }
    // Build sort fields container.
    $form['sort'] = [
      '#type' => 'container',
      '#attributes' => ['id' => 'sort-container'],
    ];
    // Build vocabularies details.
    foreach ($this->sort as $vocabularyId => $sort) {
      $open = FALSE;
      // If form_state rebuild.
      if (
        !empty($form_state->getValue('add_item_vocabulary_id')) &&
        $vocabularyId === $form_state->getValue('add_item_vocabulary_id')
      ) {
        $open = TRUE;
        // Unset form_state variable.
        $form_state->unsetValue('add_item_vocabulary_id');
      }
      elseif (
        !empty($form_state->getValue('active_sort_vocabulary_id')) &&
        $vocabularyId === $form_state->getValue('active_sort_vocabulary_id')
      ) {
        $open = TRUE;
      }
      // Load current vocabulary.
      $vocabulary = Vocabulary::load($vocabularyId);
      // Build vocabulary details wrapper.
      $form['sort']['details__' . $vocabularyId] = [
        '#type' => 'details',
        '#title' => $vocabulary->get('name'),
        '#description' => $vocabulary->get('description'),
        '#open' => $open,
        '#attributes' => ['id' => 'details--' . $vocabularyId],
      ];
      // Build an action submit.
      $form['sort']['details__' . $vocabularyId]['remove'] = [
        '#type' => 'submit',
        '#name' => 'details-remove-' . $vocabularyId,
        '#submit' => ['::submitRemoveItemForm'],
        '#ajax' => [
          'callback' => '::ajaxCallbackRemoveItemForm',
          'wrapper' => str_replace('_', '-', $this->getFormId()),
        ],
        '#value' => $this->t('Remove item @vocabulary_name', [
          '@vocabulary_name' => $vocabulary->get('name'),
        ]),
        '#attributes' => [
          // Keep this variable for evolution confirm remove.
          'data-action' => 'remove-form',
          'data-vocabulary-id' => $vocabularyId,
          'data-vocabulary-name' => $vocabulary->get('name'),
        ],
        '#button_type' => 'danger',
      ];
      // Build all items for the current vocabulary .
      $form['sort']['details__' . $vocabularyId] += $this->getSortItemFormByVocabulary(
        $vocabularyId,
        $form_state
      );
    }

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // Set variables.
    $newProperties = [];
    $vocabularyId = $form_state->getValue('property_select');
    $vocabulary = Vocabulary::load($vocabularyId);
    // Set Variable form_state.
    // Its used in ajaxCallbackAddItemForm and buildForm methods.
    $form_state->setValue('add_item_vocabulary_id', $vocabularyId);
    if (!empty($vocabulary)) {
      // Load terms of the vocabulary.
      $terms = \Drupal::entityTypeManager()
        ->getStorage('taxonomy_term')
        ->loadTree($vocabularyId);
      // For each term.
      foreach ($terms as $term) {
        // Values weight for the current term.
        foreach ($terms as $weightTerm) {
          $newProperties[$term->tid][$weightTerm->tid] = 0;
        }
      }
      // Set the new sort property.
      $this->sort[$vocabularyId] = $newProperties;
      // Update configurations.
      $hubspotConfig = $this->config('hubspot_integration.settings');
      $hubspotConfig->set('sort', $this->sort);
      $hubspotConfig->save();
      // Display success message.
      $this->messenger()->addMessage($this->t(
        'New @vocabulary_name item successfully added',
        ['@vocabulary_name' => $vocabulary->get('name')]
      ));
    }
  }

  /**
   * Remove item from sort hubspot configurations,
   *   and rebuild the current state of the form.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   */
  public function submitRemoveItemForm(array &$form, FormStateInterface $form_state) {
    // Set Variables.
    $triggeringElement = $form_state->getTriggeringElement();
    $vocabularyId = $triggeringElement['#attributes']['data-vocabulary-id'];
    $vocabularyName = $triggeringElement['#attributes']['data-vocabulary-name'];
    // Keep this variable for evolution confirm remove.
    $action = $triggeringElement['#attributes']['data-action'];
    // Set Variables form_state.
    // Its used in ajaxCallbackSortItemForm and getSortItemFormByVocabulary methods.
    $form_state->setValue('remove_item_vocabulary_id', $vocabularyId);
    $form_state->setValue('remove_item_vocabulary_name', $vocabularyName);
    // Keep this variable for evolution confirm remove.
    $form_state->setValue('remove_item_action', $action);
    // Set the new sort property.
    unset($this->sort[$vocabularyId]);
    // Update configurations.
    $hubspotConfig = $this->config('hubspot_integration.settings');
    $hubspotConfig->set('sort', $this->sort);
    $hubspotConfig->save();
    // Rebuild form_state.
    $form_state->setRebuild();
  }

  /**
   * Ajax rebuild the form structure and display message.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   *
   * @return array
   *   The form structure.
   */
  public function ajaxCallbackRemoveItemForm(array &$form, FormStateInterface $form_state) {
    // Set Variable.
    $vocabularyName = $form_state->getValue('remove_item_vocabulary_name');
    // Unset form_state variables.
    $form_state->unsetValue('remove_item_vocabulary_id')
      ->unsetValue('remove_item_vocabulary_name')
      // Keep this variable for evolution confirm remove.
      ->unsetValue('remove_item_action');
    // Display success message.
    $this->messenger()->addMessage($this->t(
      '@vocabulary_name item successfully removed',
      ['@vocabulary_name' => $vocabularyName]
    ));
    $form['messages']['status'] = [
      '#type' => 'status_messages',
    ];
    // Rebuild all the form.
    return $form;
  }

  /**
   * Build a form item depending on hubspot item type configurations.
   *
   * @param string $vocabularyId
   *   The identifier of the hubspot configuration type.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   *
   * @return array
   *   The form item structure.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  protected function getSortItemFormByVocabulary($vocabularyId, FormStateInterface $form_state) {
    // Set variables.
    $item = [];
    $terms = \Drupal::entityTypeManager()
      ->getStorage('taxonomy_term')
      ->loadTree($vocabularyId);

    // For each term.
    foreach ($terms as $term) {
      $termId = $term->tid;
      // Build fieldset for the current term.
      $item['sort__' . $termId] = [
        '#type' => 'fieldset',
        '#title' => $term->name,
      ];
      // If form_state rebuild.
      if (
        !empty($form_state->getValue('active_sort_vocabulary_id')) &&
        !empty($form_state->getValue('active_sort_term_id')) &&
        !empty($form_state->getValue('sort_action')) &&
        ($vocabularyId === $form_state->getValue('active_sort_vocabulary_id')) &&
        ($termId === $form_state->getValue('active_sort_term_id')) &&
        ('load-form' === $form_state->getValue('sort_action'))
      ) {
        // Build all fields weight for the current term.
        foreach ($terms as $weightTerm) {
          $defaultValue = 0;
          $weightTermId = $weightTerm->tid;
          if (!empty($this->sort[$vocabularyId][$termId][$weightTermId])) {
            $defaultValue = $this->sort[$vocabularyId][$termId][$weightTermId];
          }
          $weightItemName = 'voc_' . $vocabularyId . '_term_' . $termId;
          $weightItemName .= '_weight_' . $weightTermId;
          $item['sort__' . $termId][$weightItemName] = [
            '#type' => 'weight',
            '#title' => $weightTerm->name,
            '#default_value' => $defaultValue,
          ];
        }
        // Build action button(s).
        $item['sort__' . $termId]['actions'] = [
          '#type' => 'actions',
        ];
        // Build submit button for submit form.
        $item['sort__' . $termId]['actions'] += self::getItemSubmitByAction(
          'submit',
          $vocabularyId,
          $termId,
          $term->name
        );
        // Build submit button for cancel form.
        $item['sort__' . $termId]['actions'] += self::getItemSubmitByAction(
          'cancel',
          $vocabularyId,
          $termId,
          $term->name
        );
      }
      else {
        // Build action button(s).
        $item['sort__' . $termId]['actions'] = [
          '#type' => 'actions',
        ];
        // Build submit button for load form.
        $item['sort__' . $termId]['actions'] += self::getItemSubmitByAction(
          'load',
          $vocabularyId,
          $termId,
          $term->name
        );
      }
    }

    return $item;
  }

  /**
   * Build a form button based on several practical cases.
   *
   * @param string $action
   *   The item submit action type.
   * @param string $vocabularyId
   *   The vocabulary id.
   * @param string $termId
   *   The current term id.
   * @param string $termName
   *   The current term name.
   *
   * @return array
   *   The form button structure.
   */
  public function getItemSubmitByAction($action, $vocabularyId, $termId, $termName) {
    // Set variables.
    $button = [];
    $value = '';
    $button_type = '';
    switch ($action) {
      case 'load':
        $value = $this->t('Load @term_name settings', [
          '@term_name' => $termName,
        ]);
        $button_type = 'primary';
        break;

      case 'submit':
        $value = $this->t('Save @term_name settings', [
          '@term_name' => $termName,
        ]);
        $button_type = 'primary';
        break;

      case 'cancel':
        $value = $this->t('Cancel');
        $button_type = 'default';
        break;
    }
    // Build an action submit.
    $button[$action] = [
      '#type' => 'submit',
      '#name' => 'sort-' . $action . '-' . $termId,
      '#submit' => ['::submitSortItemForm'],
      '#ajax' => [
        'callback' => '::ajaxCallbackSortItemForm',
        'wrapper' => 'details--' . $vocabularyId,
      ],
      '#value' => $value,
      '#attributes' => [
        'data-action' => $action . '-form',
        'data-vocabulary-id' => $vocabularyId,
        'data-term-id' => $termId,
        'data-term-name' => $termName,
      ],
      '#button_type' => $button_type,
    ];

    return $button;
  }

  /**
   * Submit sort item form depending on several practical cases.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   */
  public function submitSortItemForm(array &$form, FormStateInterface $form_state) {
    // Set Variables.
    $triggeringElement = $form_state->getTriggeringElement();
    $vocabularyId = $triggeringElement['#attributes']['data-vocabulary-id'];
    $termId = $triggeringElement['#attributes']['data-term-id'];
    $termName = $triggeringElement['#attributes']['data-term-name'];
    $action = $triggeringElement['#attributes']['data-action'];
    // Set Variables form_state.
    // Its used in ajaxCallbackSortItemForm and getSortItemFormByVocabulary methods.
    $form_state->setValue('active_sort_vocabulary_id', $vocabularyId);
    $form_state->setValue('active_sort_term_id', $termId);
    $form_state->setValue('active_sort_term_name', $termName);
    $form_state->setValue('sort_action', $action);
    // Update Configurations if submit action.
    if ('submit-form' === $action) {
      $this->sort[$vocabularyId][$termId] = $this->getSortFromAjaxFormState($form_state);
      $hubspotConfig = $this->config('hubspot_integration.settings');
      $hubspotConfig->set('sort', $this->sort);
      $hubspotConfig->save();
      $this->messenger()->addMessage($this->t(
        'Weight configurations saved successfully for @term_name',
        ['@term_name' => $termName]
      ));
    }
    // Rebuild form_state.
    $form_state->setRebuild();
  }

  /**
   * Ajax rebuild the form item structure.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   *
   * @return array
   *   The form item structure.
   */
  public function ajaxCallbackSortItemForm(array &$form, FormStateInterface $form_state) {
    // Set variable.
    $vocabularyId = $form_state->getValue('active_sort_vocabulary_id');
    // Unset form_state variables.
    $form_state->unsetValue('active_sort_vocabulary_id')
      ->unsetValue('active_sort_term_id')
      ->unsetValue('active_sort_term_name')
      ->unsetValue('sort_action');
    // Rebuild the appropriate form fieldset.
    return $form['sort']['details__' . $vocabularyId];
  }

  /**
   * Get sort item settings from form_state values.
   *
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   *
   * @return array
   *   Sort item settings.
   */
  protected static function getSortFromAjaxFormState(FormStateInterface $form_state) {
    $sort = [];
    foreach ($form_state->getValues() as $key => $value) {
      if (preg_match('/voc_(?P<voc>\w+)_term_(?P<term>\d+)_weight_(?P<weight>\d+)/', $key, $matches)) {
        $weightTid = $matches['weight'];
        $sort[$weightTid] = $value;
      }
    }
    return $sort;
  }

}
