<?php

namespace Drupal\hubspot_integration\Services;

use Drupal\Component\Utility\UrlHelper;
use Drupal\Core\Database\Connection;
use Symfony\Component\HttpFoundation\Session\Session;
use Drupal\Core\Config\ConfigFactory;
use Drupal\taxonomy\Entity\Term;
use Drupal\taxonomy\Entity\Vocabulary;

/**
 * Hubspot API service.
 */
class HubspotAPI {

  /**
   * The database service.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $database;

  /**
   * The session service.
   *
   * @var \Symfony\Component\HttpFoundation\Session\Session
   */
  protected $session;

  /**
   * The Config Factory service.
   *
   * @var \Drupal\Core\Config\ConfigFactory
   */
  protected $configFactory;

  /**
   * Static cache for cookies.
   *
   * @var array
   */
  protected $cookies;

  /**
   * Constructs a new HubspotAPI object.
   *
   * @param \Drupal\Core\Database\Connection $connection
   *   The connection service.
   * @param \Symfony\Component\HttpFoundation\Session\Session $session
   *   The session service.
   * @param \Drupal\Core\Config\ConfigFactory $config_factory
   *   The config factory service;.
   */
  public function __construct(Connection $connection, Session $session, ConfigFactory $config_factory) {
    $this->database = $connection;
    $this->session = $session;
    $this->configFactory = $config_factory;
  }

  /**
   * Get Hubspot cookies and perform security check when necessary.
   *
   * @param string $cookieName
   *   The cookie to retrieve.
   *
   * @return string
   *   The sanitized cookie value.
   */
  public function getCookie($cookieName = 'hubspotutk') {
    if (!isset($this->cookies[$cookieName])) {
      $this->cookies[$cookieName] = \Drupal::request()->cookies->get($cookieName);
      // Ensure data in cookie is authorized / safe for hubspot_integration.
      if (($cookieName == 'hubspot_integration')
        && ($tids = explode('_', ltrim($this->cookies[$cookieName], 'hubspot_')))
        && ($mapping = $this->getMapping())
      ) {
        $allowedValues = [];
        foreach ($mapping as $map) {
          foreach ($map['#terms'] as $termId) {
            if (!empty($termId)) {
              $allowedValues[] = $termId;
            }
          }
        }
        $tids = array_intersect($tids, $allowedValues);
        $this->cookies[$cookieName] = 'hubspot_' . implode('_', $tids);
      }
    }

    return $this->cookies[$cookieName];
  }

  /**
   * Get current session.
   */
  public function getSession() {
    return $this->session;
  }

  /**
   * Determine if current user is registered as a contact on husbpot.
   *
   * @return bool
   *   Either or not the user is a contact.
   */
  public function isContact() {
    $result = FALSE;
    if ($user = $this->getContactInfo()) {
      $property = 'is-contact';
      if (isset($user->{$property})) {
        $result = $user->{$property};
      }
    }

    return $result;
  }

  /**
   * Get Husbspot values for the current user.
   *
   * @param bool $forced_update
   *   The forced update boolean.
   *
   * @return null|mixed
   *   Null.
   */
  public function getContactInfo($forced_update = FALSE) {
    $user = NULL;
    if (!$this->isDemoMode() && !$forced_update && $this->session->get('hubspot_integration.user')) {
      $user = $this->session->get('hubspot_integration.user');
    }
    else {
      $cookie = $this->getCookie();
      if (!empty($cookie)) {
        $user = $this->request("contacts/v1/contact/utk/$cookie/profile");
      }
    }
    return $user;
  }

  /**
   * Get the config name for the hubspot integration module settings.
   *
   * @param string $name
   *   The name.
   *
   * @return mixed|null|array|\Drupal\Component\Render\MarkupInterface|string|unknown[]|array[]
   *   The value.
   */
  public function getConfig(string $name) {
    return $this->configFactory->get('hubspot_integration.settings')->get($name);
  }

  /**
   * Set te config name for the hubspot integration module settings.
   *
   * @param string $name
   *   The name.
   * @param string $value
   *   The value.
   *
   * @return \Drupal\Core\Config\Config|\Drupal\Core\Config\ImmutableConfig
   *   The config.
   */
  public function setConfig(string $name, string $value) {
    return $this->configFactory->getEditable('hubspot_integration.settings')->set($name, $value)->save();
  }

  /**
   * Get the list of the possible contact properties and their definitions.
   *
   * @return mixed
   *   The contact properties.
   */
  public function getContactProperties() {
    return $this->request('properties/v1/contacts/properties');
  }

  /**
   * Function to make calls on hubspot.
   *
   * @param string $api
   *   The API.
   * @param array $options
   *   The options.
   * @param string $type
   *   The type.
   * @param array $datas
   *   Tha datas.
   *
   * @return mixed|null
   *   The response.
   */
  public function request(string $api, array $options = [], string $type = 'GET', array $datas = []) {
    $token_private_app = $this->getConfig('token_private_app');
    $headers = [
      'Content-Type' => 'application/json',
      'Authorization' => 'Bearer ' . $token_private_app,
    ];
    $ret = NULL;
    if (!empty($token_private_app)) {
      $hub_url = 'https://api.hubapi.com/';
      $query_str = UrlHelper::buildQuery($options);
      $url = $hub_url . $api . '?' . $query_str;
      try {
        switch ($type) {
          case 'GET':
            $request = \Drupal::httpClient()->get($url, ['headers' => $headers]);
            break;

          case 'POST':
            $request = \Drupal::httpClient()->post(
              $url,
              ['json' => $datas, 'headers' => $headers],
            );
            break;
        }
        $ret = json_decode($request->getBody());
      }
      catch (\Exception $e) {
        \Drupal::logger('hubspot_integration')->error('Error : ' . $e->getCode() . ' : ' . $e->getMessage());
      }
    }

    return $ret;
  }

  /**
   * Get the mapping done between Drupal and Hubspot.
   *
   * @return mixed
   *   The config.
   */
  public function getMapping() {
    return $this->getConfig('mapping');
  }

  /**
   * Get the sort configuration.
   *
   * @return mixed
   *   The config.
   */
  public function getSort($tids = []) {
    return $this->getConfig('sort');
  }

  /**
   * Get the sort configuration.
   *
   * @param array $tids
   *   The tids.
   *
   * @return array
   *   The sorted tids.
   */
  public function getTidsSort(array $tids) {
    $sort = $this->getConfig('sort');
    $tids_sort = [];
    // Compile the sort in order to get the min values for the tids.
    if (!empty($tids[0])) {
      foreach ($tids as $tid) {
        /** @var \Drupal\taxonomy\TermInterface $term */
        $term = Term::load($tid);
        $vid = $term->bundle();
        foreach ($sort[$vid][$tid] as $term_weight_id => $term_weight) {
          if (!isset($tids_sort[$term_weight_id])) {
            $tids_sort[$term_weight_id] = $sort[$vid][$tid][$term_weight_id];
          }
          else {
            $tids_sort[$term_weight_id] = min($tids_sort[$term_weight_id], $sort[$vid][$tid][$term_weight_id]);
          }
        }
      }
    }

    return $tids_sort;
  }

  /**
   * Utility function to transform a text as a machine name.
   *
   * @param string $string
   *   The string.
   *
   * @return string
   *   The name.
   */
  public static function machineName(string $string) {
    return preg_replace('@[^a-z0-9-]+@', '-', strtolower($string));
  }

  /**
   * Utility function to check a vocabulary is mapped with hubspot.
   *
   * @param string $vocabulary
   *   The vocabulary.
   *
   * @return bool
   *   The boolean.
   */
  public function isVocabularyMapped($vocabulary) {
    $mapping = $this->getMapping();
    if (!empty($mapping)) {
      foreach ($mapping as $mapping) {
        if (isset($mapping['#vocabulary']) && $mapping['#vocabulary'] === $vocabulary) {
          return TRUE;
        }
      }
    }

    return FALSE;
  }

  /**
   * Utility function to get mapped vocabularies.
   *
   * @return array
   *   The list of vocabularies.
   */
  public function getMappedVocabularies() {
    $vocabularies = Vocabulary::loadMultiple();
    $vocabulariesList = [];
    foreach ($vocabularies as $vid => $vocablary) {
      if ($this->isVocabularyMapped($vid)) {
        $vocabulariesList[$vid] = $vocablary->get('name');
      }
    }

    return $vocabulariesList;
  }

  /**
   * Retrieve the demo mode config.
   */
  public function isDemoMode() {
    return $this->getConfig('demo_mode');
  }

  /**
   * Retrieve the anonymous_contact config.
   */
  public function anonymousContactCreationEnabled() {
    return $this->getConfig('anonymous_contact');
  }

  /**
   * Display a message.
   *
   * @param string $message
   *   The message.
   */
  public function demoMessage(string $message) {
    if ($this->isDemoMode()) {
      \Drupal::messenger()->addStatus("Demo Hubspot : $message");
    }
  }

  /**
   * Get user terms.
   *
   * @return array
   *   The term ids.
   */
  public function getUserTids() {
    $tids = [];
    if (($hub_user = $this->getContactInfo())
      && ($mapping = $this->getMapping())
      && !empty($mapping)
    ) {
      $taxonomy = [];
      foreach ($mapping as $hub_voc => $map) {
        if (isset($hub_user->properties->{$hub_voc})) {
          foreach ($map['#terms'] as $hub_term_id => $taxonomy_term_id) {
            if ($taxonomy_term_id && isset($hub_user->properties->{$hub_voc}->value) && HubspotAPI::machineName($hub_user->properties->{$hub_voc}->value) == $hub_term_id) {
              $taxonomy_term = Term::load($taxonomy_term_id);
              $taxonomy[$taxonomy_term->id()] = $taxonomy_term->bundle();
            }
          }
        }
      }
      foreach ($taxonomy as $tid => $vocab) {
        $tids[] = $tid;
      }
    }

    return $tids;
  }

  /**
   * Get the number of anonymous contact.
   *
   * @param bool $update
   *   The update status.
   *
   * @return array|array[]|\Drupal\Component\Render\MarkupInterface|\Drupal\hubspot_integration\Services\unknown[]|int|mixed|string|null
   *   The anonymous contact count.
   */
  public function getAnonymousContactNumber(bool $update = FALSE) {
    if ($update) {
      $request = $this->request('contacts/v1/search/query', ['q' => $this->getAnonymousContactMailTemplate('*')]);
      if (!empty($request)) {
        $this->setConfig('anonymous_contacts_count', $request->total);
        return $request->total;
      }
      else {
        return 0;
      }
    }
    else {
      return $this->getConfig('anonymous_contacts_count');
    }
  }

  /**
   * Get the mail template for anonymous.
   *
   * @param string|null $hub_cookie
   *   The hub cookie.
   *
   * @return array|array[]|\Drupal\Component\Render\MarkupInterface|\Drupal\hubspot_integration\Services\unknown[]|mixed|string|string[]|null
   *   The template.
   */
  public function getAnonymousContactMailTemplate(string $hub_cookie = NULL) {
    $template = $this->getConfig('anonymous_mail_template');
    if (!empty($hub_cookie)) {
      $template = str_replace('[hub_cookie]', $hub_cookie, $template);
    }

    return $template;
  }

}
